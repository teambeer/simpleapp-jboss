package org.udg.pds.simpleapp_jboss.service;

import java.util.Date;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.udg.pds.simpleapp_jboss.model.Task;
import org.udg.pds.simpleapp_jboss.model.User;

@Stateful
@LocalBean
public class TaskService {
    @PersistenceContext
    private EntityManager em;

    @XmlRootElement(name="collection")
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class TaskList {
        public List<Task> tasks;
    }

    public TaskList getTasks(Long id) {
        Query q =  em.createQuery("select t from Task t where t.userId = :userId order by t.dateCreated");
        q.setParameter("userId", id);
        TaskList tl = new TaskList();
        tl.tasks = q.getResultList();
        return tl;
    }

    public Task getTask(Long id) throws Exception {
        return em.find(Task.class, id);
    }

    public Task addTask(String text, Long userId,
                        Date created, Date limit) throws Exception {

        User user = em.find(User.class, userId);

        Task task = new Task();

        task.setText(text);
        task.setUser(user);
        task.setDateCreated(created);
        task.setDateLimit(limit);
        task.setCompleted(false);

        em.persist(task);
        return task;
    }
}
