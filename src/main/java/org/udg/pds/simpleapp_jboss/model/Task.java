package org.udg.pds.simpleapp_jboss.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.hibernate.validator.constraints.NotEmpty;
import org.udg.pds.simpleapp_jboss.util.DateAdapter;
import org.udg.pds.simpleapp_jboss.util.LongAdapter;

@Entity
@XmlRootElement
// This tells JAXB that it has to ignore getters and setters and only use fields for XML marshaling/unmarshaling
@XmlAccessorType(XmlAccessType.FIELD)
public class Task implements Serializable {
    /** Default value included to remove warning. Remove or modify at will. **/
    private static final long serialVersionUID = 1L;

    // This tells JAXB that this field can be used as ID
    @XmlID
    // Since XmlID can only be used on Strings, we need to use LongAdapter to transform Long <-> String
    @XmlJavaTypeAdapter(LongAdapter.class)
    @Id
    // Don't forget to use the extra argument "strategy = GenerationType.IDENTITY" to get AUTO_INCREMENT
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // We are using a DateAdapter to control how dates are translated to and from XML
    @XmlJavaTypeAdapter(DateAdapter.class)
    @NotNull
    private Date dateCreated;

    @XmlJavaTypeAdapter(DateAdapter.class)
    @NotNull
    private Date dateLimit;

    @NotNull
    private Boolean completed;

    @NotNull
    @NotEmpty
    private String text;

    @Column(name = "ixUser", insertable = false, updatable = false)
    private Long userId;

    // We use this to tell JAXB to ignore this field. Otherwise it will try to fetch a User object
    // every time a Task object is translated to XML
    @XmlTransient
    @NotNull
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "ixUser")
    private User user;

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public String getText() { return text; }
    public void setText(String s) { text = s; }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateLimit() {
        return dateLimit;
    }

    public void setDateLimit(Date dateLimit) {
        this.dateLimit = dateLimit;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
