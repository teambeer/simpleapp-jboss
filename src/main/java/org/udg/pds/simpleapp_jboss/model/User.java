package org.udg.pds.simpleapp_jboss.model;

import org.hibernate.validator.constraints.NotEmpty;
import org.udg.pds.simpleapp_jboss.util.LongAdapter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.*;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: imartin
 * Date: 9/03/13
 * Time: 20:32
 * To change this template use File | Settings | File Templates.
 */


import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@Entity
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"email", "username"}))
public class User implements Serializable {
    /**
     * Default value included to remove warning. Remove or modify at will. *
     */
    private static final long serialVersionUID = 1L;

    @XmlID
    @XmlJavaTypeAdapter(LongAdapter.class)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @NotNull
    private String username;

    @NotNull
    private String email;

    @XmlTransient
    @NotNull
    @NotEmpty
    private String password;

    @XmlTransient
    @OneToMany(cascade = CascadeType.ALL, fetch=FetchType.LAZY,
               mappedBy = "user", targetEntity = Task.class)
    private List<Task> tasks;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> ts) {
        this.tasks = ts;
    }
}
