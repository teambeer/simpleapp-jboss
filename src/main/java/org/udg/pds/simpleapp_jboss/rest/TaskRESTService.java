package org.udg.pds.simpleapp_jboss.rest;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.xml.bind.JAXBContext;

import org.udg.pds.simpleapp_jboss.model.Task;
import org.udg.pds.simpleapp_jboss.model.User;
import org.udg.pds.simpleapp_jboss.service.TaskService;

@Path("/task")
@RequestScoped
public class TaskRESTService {
    @EJB
    TaskService taskService;

    @GET
    @Path("{id}")
    @Produces("text/xml")
    public String getTask(@Context HttpServletRequest req,
                          @PathParam("id") Long id) {
        HttpSession session = req.getSession();

        if (session == null) {
            return Error.build("Sessions not supported!");
        }
        Long userId = (Long) session.getAttribute("simpleapp_auth_id");
        if (userId == null) {
            return Error.build("User not authenticated!");
        }

        try {
            Task t = taskService.getTask(id);
            // Check that the user authenticated in the session owns the task it is trying to access
            if (t.getUserId() != userId) {
                return Error.build("You don't own this task!");
            }
            StringWriter sw = new StringWriter();
            JAXBContext.newInstance(Task.class).createMarshaller().marshal(t, sw);
            return sw.toString();
        }
        catch (Exception ex) {
            return Error.build(ex.getMessage());
        }
    }

    @GET
    @Path("/all")
    @Produces("text/xml")
    public String listAllTasks(@Context HttpServletRequest req) {
        HttpSession session = req.getSession();

        if (session == null) {
            return Error.build("Sessions not supported!");
        }
        Long userId = (Long) session.getAttribute("simpleapp_auth_id");
        if (userId == null) {
            return Error.build("User not authenticated!");
        }

        final TaskService.TaskList results = taskService.getTasks(userId);
        StringWriter sw = new StringWriter();
        try {
            JAXBContext.newInstance(TaskService.TaskList.class).createMarshaller().marshal(results, sw);
            return sw.toString();
        } catch (Exception ex) {
            return Error.build(ex.getMessage());
        }
    }

    @POST
    @Path("/add")
    @Produces("text/xml")
    public String addTask(@Context HttpServletRequest req, @FormParam("text") String text,
                          @FormParam("dateCreated") Date dateCreated, @FormParam("dateLimit") Date dateLimit) {

        HttpSession session = req.getSession();

        if (session == null) {
            return Error.build("Sessions not supported!");
        }
        Long userId = (Long) session.getAttribute("simpleapp_auth_id");
        if (userId == null) {
            return Error.build("User not authenticated!");
        }

        if (text == null) {
            return Error.build("No text supplied");
        }
        if (dateCreated == null) {
            return Error.build("No creation date supplied");
        }
        if (dateLimit == null) {
            return Error.build("No limit date supplied");
        }

        try {
            Task t = taskService.addTask(text, userId, dateCreated, dateLimit);
            StringWriter sw = new StringWriter();
            JAXBContext.newInstance(Task.class).createMarshaller().marshal(t, sw);
            return sw.toString();
        } catch (Exception ex) {
            return Error.build(ex.getMessage());
        }
    }
}
