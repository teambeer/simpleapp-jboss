package org.udg.pds.simpleapp_jboss.rest;

import org.udg.pds.simpleapp_jboss.model.Task;
import org.udg.pds.simpleapp_jboss.model.User;
import org.udg.pds.simpleapp_jboss.service.UserService;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Path;
import javax.ws.rs.POST;
import javax.ws.rs.FormParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.bind.JAXBContext;
import java.io.StringWriter;

// This class is used to process all the authentication related URLs
@Path("/auth")
@RequestScoped
public class AuthRESTService {
    // This is the EJB used to access user data
    @EJB
    UserService userService;

    @POST
    @Produces("text/xml")
    public String auth(
            @Context HttpServletRequest req,
            @FormParam("username") String username,
            @FormParam("password") String password) {

        // Access to the HTTP session
        HttpSession session = req.getSession();

        if (session == null) {
            return Error.build("Sessions not supported!");
        }

        // Check if the session has the attribute "simpleapp_auth_id"
        if (session.getAttribute("simpleapp_auth_id") == null) {
            // If the user is not authenticated we have to check if the password match
            User u = userService.matchPassword(username, password);
            if (u != null) {
                try {
                    // The username and password match, add the "simpleapp_auth_id" attribute to the session
                    // to identify the user in the next calls
                    session.setAttribute("simpleapp_auth_id", u.getId());
                    StringWriter sw = new StringWriter();
                    JAXBContext.newInstance(User.class).createMarshaller().marshal(u, sw);
                    return sw.toString();
                }
                catch (Exception ex) {
                    return Error.build(ex.getMessage());
                }
            }
            else
                return Error.build("Authentication error!");
        }
        else {
            return Error.build("Already authenticated!");
        }
    }

    @Path("/register")
    @POST
    @Produces("text/xml")
    public String register(
            @Context HttpServletRequest req,
            @FormParam("username") String username,
            @FormParam("email") String email,
            @FormParam("password") String password) {

        HttpSession session = req.getSession();

        if (session == null) {
            return Error.build("Sessions not supported!");
        }

        if (session.getAttribute("simpleapp_auth_id") != null) {
            return Error.build("You are already authenticated!");
        }

        User u = userService.register(username, email, password);
        if (u != null) {
            try {
            StringWriter sw = new StringWriter();
            JAXBContext.newInstance(User.class).createMarshaller().marshal(u, sw);
            return sw.toString();
            } catch (Exception ex) {
                return Error.build(ex.getMessage());
            }
        }

        return Error.build("Your email is already registered!");

    }
}
